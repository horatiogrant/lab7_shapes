/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab7wrist;
//import Lab7wrist.wristband;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author grant
 */
public class patient {
    Scanner kb = new Scanner (System.in);
    private String name;
    private int dateOfBirth;
    private String familyDoc;
    public ArrayList <wristband> bands = new ArrayList <wristband>();
    private String ResearchGroup;

    public patient(String name, int dateOfBirth, String familyDoc) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoc = familyDoc;
        
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFamilyDoc() {
        return familyDoc;
    }

    public void setFamilyDoc(String familyDoc) {
        this.familyDoc = familyDoc;
    }

  
  
    public ArrayList<wristband> getBands() {
        return bands;
    }

    public void setBands(ArrayList<wristband> bands) {
        this.bands = bands;
    }

    public String getResearchGroup() {
        return ResearchGroup;
    }

    public void setResearchGroup(String ResearchGroup) {
        this.ResearchGroup = ResearchGroup;
    }
    
     public void addBand(){
        //System.out.print("here");
         System.out.println("Do you want to add a normal (1), child(2), or allergy(3) band?");
         int in = kb.nextInt();
          
         if (in==3){
             bands.add( new allergyBand(101, "details", "pennicilyn")) ;
         }else if(in==2){
             bands.add(new childBand(101, "details", "Mom"))  ;
         }else{
             bands.add(new wristband(101, "details"))  ;
         }
        //bands.add();
    } 
    public void removeBand(wristband band){
        this.bands.remove(band);
    }
   
}
