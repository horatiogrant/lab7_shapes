/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Veggies;

/**
 *
 * @author grant
 */
abstract class Veggies {
    private String colour;
    private double size;

    public Veggies(String colour, double size) {
        this.colour = colour;
        this.size = size;
    }

    public String getColour() {
        return colour;
    }

    public double getSize() {
        return size;
    }
    
    public void isRipe(){
        
    }
    
  
}
