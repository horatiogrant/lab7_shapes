/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Shape;

import static java.lang.Math.PI;

/**
 *
 * @author grant
 */
public class Circle extends Shape {
    
    
  public double getArea(double d){
      d= PI *(d*d);
        return d;
    }
     public double getPerimeter(double d){ 
         d=2*(PI*d);
       return d;
    }
    
}
